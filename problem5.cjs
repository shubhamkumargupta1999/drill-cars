// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const problem4 = require("./problem4.cjs");
const inventory = require('./inventory.cjs');

const answer4 = problem4(inventory);

function problem5(answer4) {

    if (arguments.length < 1 || answer4.length === 0 || !Array.isArray(answer4)) {
        return [];
    }

    const olderCarsList = [];

    for (let count = 0; count < answer4.length; count++) {
        if (answer4[count] < 2000) {
            olderCarsList.push(answer4[count]);
        }
    }
    return olderCarsList.length;
}

module.exports = problem5;
