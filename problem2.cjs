// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

const inventory = require('./inventory.cjs');

const last = inventory.length - 1;

function problem2(inventory) {

    if (arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    return inventory[last];
}

module.exports = problem2;
